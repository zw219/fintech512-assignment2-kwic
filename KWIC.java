import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class KWIC {

    private String address;
    public KWIC(String path) {
        this.address = path;
    }

    public String run() throws IOException {
        // create list
        List<String> ignore = new ArrayList<String>();
        List<String> key = new ArrayList<String>();
        List<String> sentence = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        // set flag, 1 for ignore read, -1 for sentence read
        int flag = 1;
        try {
            File myfile = new File(address);
            Scanner myReader = new Scanner(myfile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine().toLowerCase();
                if (data.equals("::")) {
                    flag = -1;
                    continue;
                }
                if (flag == 1) {
                    ignore.add(data);
                } else {
                    sentence.add(data);
                }
            }
            if (flag == 1) {
                // throw exceptions
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        for (int i = 0; i < sentence.size(); i++) {
            String line = sentence.get(i);
            String[] words = line.split(" ");
            for (int j = 0; j < words.length; j++) {
                String word = words[j];
                if (!ignore.contains(word) && !key.contains(word)) {
                    key.add(word);
                }
            }
        }
        Collections.sort(key);
        for (int i = 0; i < key.size(); i++) {
            String keyword = key.get(i);
            for (int j = 0; j < sentence.size(); j++) {
                String s = sentence.get(j);
                String[] words = s.split(" ");
                List<Integer> indexes = new ArrayList<Integer>();
                for (int k = 0; k < words.length; k++) {
                    if (words[k].equals(keyword)) {
                        indexes.add(k);
                    }
                }
                for (int k = 0; k < indexes.size(); k++) {
                    for (int l = 0; l < words.length; l++) {
                        if (indexes.get(k) == l) {
                            sb.append(words[l].toUpperCase());
                        } else {
                            sb.append(words[l]);
                        }
                        if (l != words.length - 1) {
                            sb.append(" ");
                        }
                    }
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        // for test use
        KWIC k = new KWIC("./input.txt");
        System.out.println(k.run());
    }
}
