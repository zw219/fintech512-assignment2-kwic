import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;

import java.io.IOException;

public class KWICTEST {
    @Test
    void testInput1() throws IOException {
        String expected = "a portrait of the ARTIST as a young man\n" + "the ASCENT of man\n" +
                "a man is a man but BUBBLESORT is a dog\n" +
                "DESCENT of man\n" +
                "a man is a man but bubblesort is a DOG\n" +
                "descent of MAN\n" +
                "the ascent of MAN\n" +
                "the old MAN and the sea\n" +
                "a portrait of the artist as a young MAN\n" +
                "a MAN is a man but bubblesort is a dog\n" +
                "a man is a MAN but bubblesort is a dog\n" +
                "the OLD man and the sea\n" +
                "a PORTRAIT of the artist as a young man\n" +
                "the old man and the SEA\n" +
                "a portrait of the artist as a YOUNG man\n";
        assertEquals(new KWIC("./input.txt").run(), expected);
    }
}

